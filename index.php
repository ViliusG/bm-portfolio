<?php
include 'tools.php';
error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Benas Matulis</title>
	<meta charset="utf-8">
	<script src="https://use.fontawesome.com/8bbe5bb0ea.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,800,900" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<div class="container">
	<div class="first-page page">
		<div class="intro">
				<p id="hey">Hey,</p>
				<p id="name">my name is Ben</p>
				<p id="peeing" class="right">And yes. I'm peeing here.</p>
		</div>
		<div class="scroll">
			<p>Scroll down <span id="arrow-down"><i class="fa fa-arrow-down" aria-hidden="true"></i>
</span></p>
		</div>
	</div>
	<div class="second-page page">
		<div class="arrow left">
			<div>
				<p><i class="fa fa-arrow-down" aria-hidden="true"></i></p>
			</div>
		</div>
		<div class="design-logos left">
			<div class="design-n-shit">
				<p class="IDesign">I design</p>
				<p class="nshit">logo's n shit.</p>
			</div>
			<div class="face">
				<img src="media/face.png">
			</div>
		</div>
	</div>
	<div class="third-page page">
		<?php
			$sql = "SELECT * FROM logo;";
			$result = mysqli_query($conn, $sql);
			$rows = mysqli_fetch_all($result);
			foreach ($rows as $row):
			?>
			<?php if ($row[2] == 1): ?> <!-- in the sql table this is activity. deafult is 1, therefore it will apear, if you set it to 0, it will dissapear, or you might as well delete it -->
				<div class="logo left">
					<img src="<?php echo $row[1]; ?>">
				</div>
		<?php 	
			endif;
			endforeach;
		?>
		
	</div>
	<div class="contact page">
		<div class="hitmeup">
			<p class="IDesign">Hit me up</p>
			<p class="nshit">if you want one.</p>
		</div>
		<?php include('form-process.php') ?>
		<form action="<?php $_SERVER['PHP_SELF']; ?>" id="user-form" method="post">
			<input class="input email" type="email" name="email" placeholder="Your email address."	>
			<span class="error"> <?php echo($email_error) ?> </span>
			<textarea class="input about" type="text" cols="40" rows="5"  name="text" placeholder="Tell me about your project."><?php 
					if ($text !== "") {
						echo($text); 
					}
				?></textarea>
			<span class="error"><?php echo $text_error ?></span>
			<input class="input submit" type="submit" name="submit" value="Send!">
		</form>
		<!-- <textarea form="user-form" class="input email"></textarea> -->
	</div>
</div>

<!-- SCRIPTS *************************************************************************************************8 -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>